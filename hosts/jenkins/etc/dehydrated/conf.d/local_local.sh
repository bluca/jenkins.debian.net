# shellcheck shell=sh disable=SC2034

CA="https://acme-v02.api.letsencrypt.org/directory"
#CA="https://acme-staging-v02.api.letsencrypt.org/directory"

RENEW_DAYS="30"
KEYSIZE="4096"
PRIVATE_KEY_RENEW="no"

CHALLENGETYPE="http-01"
HOOK="/etc/dehydrated/hooks.sh"

CONTACT_EMAIL="contact@reproducible-builds.org"
